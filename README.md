## Tutorial 
https://docs.microsoft.com/en-us/aspnet/core/data/ef-mvc/intro

## Prerequisite
NET Core 2.0.0 SDK or later.
Visual Studio 2017 version 15.3 or later with the ASP.NET and web development workload.